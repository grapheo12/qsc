package tlc

import (
	"context"
	"fmt"
	"qsc/pkg/network"
	"qsc/pkg/rpc"
	"sync/atomic"
)

type TLCR struct {
	tr int

	qId    int32
	nodeId int32
	n      *network.Network
	ch     network.Queue
	mainCh network.Queue
	drop   int32

	LogR [][]*rpc.TLCRAtom

	End context.CancelFunc
}

func (t *TLCR) msgBuffer(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case data := <-t.ch:
			_drop := atomic.LoadInt32(&t.drop)
			if _drop == 0 {
				fmt.Println("Node", t.nodeId, "| Message received")
				t.mainCh <- data
			} else {
				fmt.Println("Node", t.nodeId, "| Message dropped")
			}

		}
	}
}

func (t *TLCR) Init(_tr int, qId int32, n *network.Network) error {
	t.tr = _tr

	t.qId = qId
	t.nodeId = n.NodeId
	t.n = n

	t.ch = make(network.Queue)
	err := n.RegisterQueue(qId, t.ch)

	if err != nil {
		return err
	}

	t.LogR = make([][]*rpc.TLCRAtom, 0)

	t.mainCh = make(network.Queue)
	t.drop = 1

	ctx, _end := context.WithCancel(context.Background())
	t.End = _end
	go t.msgBuffer(ctx)

	return nil
}

func (t *TLCR) Broadcast(m []byte) ([]*rpc.TLCRAtom, error) {
	atomic.StoreInt32(&t.drop, 0)
	defer atomic.StoreInt32(&t.drop, 1)

	t.LogR = append(t.LogR, make([]*rpc.TLCRAtom, 0))
	msg := rpc.TLCRBroadcaster{
		Current: &rpc.TLCRAtom{
			Id:   t.nodeId,
			Msg:  m,
			Time: int32(len(t.LogR)),
		},
		LastR: make([]*rpc.TLCRAtom, 0),
	}

	if len(t.LogR) > 1 {
		msg.LastR = t.LogR[len(t.LogR)-1]
	}

	b, err := msg.Marshal()
	if err != nil {
		return nil, err
	}

	t.LogR[len(t.LogR)-1] = append(t.LogR[len(t.LogR)-1], msg.Current)

	t.n.Broadcast(t.qId, b)

	for len(t.LogR[len(t.LogR)-1]) < t.tr {
		nodeMsg := <-t.mainCh
		oMsg := rpc.TLCRBroadcaster{}
		err := oMsg.Unmarshal(nodeMsg.Data)
		if err != nil {
			fmt.Println("Node", t.n.NodeId, "| TLCR Error:", err.Error())
			continue
		}

		if oMsg.Current.Time == msg.Current.Time {
			t.LogR[len(t.LogR)-1] = append(t.LogR[len(t.LogR)-1], oMsg.Current)
		} else if oMsg.Current.Time > msg.Current.Time {
			// Viral adoption
			t.LogR[len(t.LogR)-1] = append(t.LogR[len(t.LogR)-1], oMsg.LastR...)
		}
	}

	return t.LogR[len(t.LogR)-1], nil
}
