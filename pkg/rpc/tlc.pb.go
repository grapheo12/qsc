// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: tlc.proto

package rpc

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type TLCRAtom struct {
	Id                   int32    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Msg                  []byte   `protobuf:"bytes,2,opt,name=msg,proto3" json:"msg,omitempty"`
	Time                 int32    `protobuf:"varint,3,opt,name=time,proto3" json:"time,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TLCRAtom) Reset()         { *m = TLCRAtom{} }
func (m *TLCRAtom) String() string { return proto.CompactTextString(m) }
func (*TLCRAtom) ProtoMessage()    {}
func (*TLCRAtom) Descriptor() ([]byte, []int) {
	return fileDescriptor_3f459bfe7eb581f8, []int{0}
}
func (m *TLCRAtom) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *TLCRAtom) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_TLCRAtom.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *TLCRAtom) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TLCRAtom.Merge(m, src)
}
func (m *TLCRAtom) XXX_Size() int {
	return m.Size()
}
func (m *TLCRAtom) XXX_DiscardUnknown() {
	xxx_messageInfo_TLCRAtom.DiscardUnknown(m)
}

var xxx_messageInfo_TLCRAtom proto.InternalMessageInfo

func (m *TLCRAtom) GetId() int32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *TLCRAtom) GetMsg() []byte {
	if m != nil {
		return m.Msg
	}
	return nil
}

func (m *TLCRAtom) GetTime() int32 {
	if m != nil {
		return m.Time
	}
	return 0
}

type TLCRBroadcaster struct {
	Current              *TLCRAtom   `protobuf:"bytes,1,opt,name=current,proto3" json:"current,omitempty"`
	LastR                []*TLCRAtom `protobuf:"bytes,2,rep,name=lastR,proto3" json:"lastR,omitempty"`
	XXX_NoUnkeyedLiteral struct{}    `json:"-"`
	XXX_unrecognized     []byte      `json:"-"`
	XXX_sizecache        int32       `json:"-"`
}

func (m *TLCRBroadcaster) Reset()         { *m = TLCRBroadcaster{} }
func (m *TLCRBroadcaster) String() string { return proto.CompactTextString(m) }
func (*TLCRBroadcaster) ProtoMessage()    {}
func (*TLCRBroadcaster) Descriptor() ([]byte, []int) {
	return fileDescriptor_3f459bfe7eb581f8, []int{1}
}
func (m *TLCRBroadcaster) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *TLCRBroadcaster) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_TLCRBroadcaster.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *TLCRBroadcaster) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TLCRBroadcaster.Merge(m, src)
}
func (m *TLCRBroadcaster) XXX_Size() int {
	return m.Size()
}
func (m *TLCRBroadcaster) XXX_DiscardUnknown() {
	xxx_messageInfo_TLCRBroadcaster.DiscardUnknown(m)
}

var xxx_messageInfo_TLCRBroadcaster proto.InternalMessageInfo

func (m *TLCRBroadcaster) GetCurrent() *TLCRAtom {
	if m != nil {
		return m.Current
	}
	return nil
}

func (m *TLCRBroadcaster) GetLastR() []*TLCRAtom {
	if m != nil {
		return m.LastR
	}
	return nil
}

func init() {
	proto.RegisterType((*TLCRAtom)(nil), "rpc.TLCRAtom")
	proto.RegisterType((*TLCRBroadcaster)(nil), "rpc.TLCRBroadcaster")
}

func init() { proto.RegisterFile("tlc.proto", fileDescriptor_3f459bfe7eb581f8) }

var fileDescriptor_3f459bfe7eb581f8 = []byte{
	// 182 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x2c, 0xc9, 0x49, 0xd6,
	0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x2e, 0x2a, 0x48, 0x56, 0x72, 0xe0, 0xe2, 0x08, 0xf1,
	0x71, 0x0e, 0x72, 0x2c, 0xc9, 0xcf, 0x15, 0xe2, 0xe3, 0x62, 0xca, 0x4c, 0x91, 0x60, 0x54, 0x60,
	0xd4, 0x60, 0x0d, 0x62, 0xca, 0x4c, 0x11, 0x12, 0xe0, 0x62, 0xce, 0x2d, 0x4e, 0x97, 0x60, 0x52,
	0x60, 0xd4, 0xe0, 0x09, 0x02, 0x31, 0x85, 0x84, 0xb8, 0x58, 0x4a, 0x32, 0x73, 0x53, 0x25, 0x98,
	0xc1, 0x6a, 0xc0, 0x6c, 0xa5, 0x78, 0x2e, 0x7e, 0x90, 0x09, 0x4e, 0x45, 0xf9, 0x89, 0x29, 0xc9,
	0x89, 0xc5, 0x25, 0xa9, 0x45, 0x42, 0xea, 0x5c, 0xec, 0xc9, 0xa5, 0x45, 0x45, 0xa9, 0x79, 0x25,
	0x60, 0xd3, 0xb8, 0x8d, 0x78, 0xf5, 0x8a, 0x0a, 0x92, 0xf5, 0x60, 0x16, 0x05, 0xc1, 0x64, 0x85,
	0x94, 0xb9, 0x58, 0x73, 0x12, 0x8b, 0x4b, 0x82, 0x24, 0x98, 0x14, 0x98, 0x31, 0x95, 0x41, 0xe4,
	0x9c, 0x04, 0x4e, 0x3c, 0x92, 0x63, 0xbc, 0xf0, 0x48, 0x8e, 0xf1, 0xc1, 0x23, 0x39, 0xc6, 0x19,
	0x8f, 0xe5, 0x18, 0x92, 0xd8, 0xc0, 0x1e, 0x30, 0x06, 0x04, 0x00, 0x00, 0xff, 0xff, 0x93, 0xf5,
	0xad, 0xe4, 0xcd, 0x00, 0x00, 0x00,
}

func (m *TLCRAtom) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *TLCRAtom) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *TLCRAtom) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	if m.Time != 0 {
		i = encodeVarintTlc(dAtA, i, uint64(m.Time))
		i--
		dAtA[i] = 0x18
	}
	if len(m.Msg) > 0 {
		i -= len(m.Msg)
		copy(dAtA[i:], m.Msg)
		i = encodeVarintTlc(dAtA, i, uint64(len(m.Msg)))
		i--
		dAtA[i] = 0x12
	}
	if m.Id != 0 {
		i = encodeVarintTlc(dAtA, i, uint64(m.Id))
		i--
		dAtA[i] = 0x8
	}
	return len(dAtA) - i, nil
}

func (m *TLCRBroadcaster) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *TLCRBroadcaster) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *TLCRBroadcaster) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.XXX_unrecognized != nil {
		i -= len(m.XXX_unrecognized)
		copy(dAtA[i:], m.XXX_unrecognized)
	}
	if len(m.LastR) > 0 {
		for iNdEx := len(m.LastR) - 1; iNdEx >= 0; iNdEx-- {
			{
				size, err := m.LastR[iNdEx].MarshalToSizedBuffer(dAtA[:i])
				if err != nil {
					return 0, err
				}
				i -= size
				i = encodeVarintTlc(dAtA, i, uint64(size))
			}
			i--
			dAtA[i] = 0x12
		}
	}
	if m.Current != nil {
		{
			size, err := m.Current.MarshalToSizedBuffer(dAtA[:i])
			if err != nil {
				return 0, err
			}
			i -= size
			i = encodeVarintTlc(dAtA, i, uint64(size))
		}
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func encodeVarintTlc(dAtA []byte, offset int, v uint64) int {
	offset -= sovTlc(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *TLCRAtom) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.Id != 0 {
		n += 1 + sovTlc(uint64(m.Id))
	}
	l = len(m.Msg)
	if l > 0 {
		n += 1 + l + sovTlc(uint64(l))
	}
	if m.Time != 0 {
		n += 1 + sovTlc(uint64(m.Time))
	}
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func (m *TLCRBroadcaster) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.Current != nil {
		l = m.Current.Size()
		n += 1 + l + sovTlc(uint64(l))
	}
	if len(m.LastR) > 0 {
		for _, e := range m.LastR {
			l = e.Size()
			n += 1 + l + sovTlc(uint64(l))
		}
	}
	if m.XXX_unrecognized != nil {
		n += len(m.XXX_unrecognized)
	}
	return n
}

func sovTlc(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozTlc(x uint64) (n int) {
	return sovTlc(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *TLCRAtom) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowTlc
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: TLCRAtom: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: TLCRAtom: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Id", wireType)
			}
			m.Id = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowTlc
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Id |= int32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Msg", wireType)
			}
			var byteLen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowTlc
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				byteLen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if byteLen < 0 {
				return ErrInvalidLengthTlc
			}
			postIndex := iNdEx + byteLen
			if postIndex < 0 {
				return ErrInvalidLengthTlc
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Msg = append(m.Msg[:0], dAtA[iNdEx:postIndex]...)
			if m.Msg == nil {
				m.Msg = []byte{}
			}
			iNdEx = postIndex
		case 3:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Time", wireType)
			}
			m.Time = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowTlc
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Time |= int32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		default:
			iNdEx = preIndex
			skippy, err := skipTlc(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthTlc
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *TLCRBroadcaster) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowTlc
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: TLCRBroadcaster: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: TLCRBroadcaster: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Current", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowTlc
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthTlc
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthTlc
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			if m.Current == nil {
				m.Current = &TLCRAtom{}
			}
			if err := m.Current.Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field LastR", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowTlc
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthTlc
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthTlc
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.LastR = append(m.LastR, &TLCRAtom{})
			if err := m.LastR[len(m.LastR)-1].Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipTlc(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthTlc
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			m.XXX_unrecognized = append(m.XXX_unrecognized, dAtA[iNdEx:iNdEx+skippy]...)
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipTlc(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowTlc
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowTlc
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowTlc
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthTlc
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupTlc
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthTlc
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthTlc        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowTlc          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupTlc = fmt.Errorf("proto: unexpected end of group")
)
