package main

import (
	"fmt"
	"qsc/pkg/network"
	"qsc/pkg/tlc"
	"sync"
	"time"
)

func worker(t tlc.TLCR, nodeId int) {
	for i := 0; i < 400; i++ {
		Rset, err := t.Broadcast([]byte(fmt.Sprintf("#%d", i)))
		if err != nil {
			fmt.Println("Node", nodeId, "| Iter:", i, "Error:", err.Error())
			continue
		}
		fmt.Println("Node", nodeId, "| Iter:", i, "Rset:", Rset)
	}
}

func main() {
	ports := []string{":2022", ":3022", ":4022", ":5022", ":6022"}
	nets := make([]*network.Network, len(ports))

	for i, p := range ports {
		nets[i] = &network.Network{}
		err := nets[i].Init(p, int32(i))
		if err != nil {
			fmt.Println(err.Error())
		}
	}
	time.Sleep(200 * time.Microsecond)

	for i := range ports {
		for j, p2 := range ports {
			if i != j {
				_, err := nets[i].Connect(int32(j), "127.0.0.1"+p2)
				if err != nil {
					fmt.Println(err.Error())
				}
			}
		}
	}
	time.Sleep(200 * time.Microsecond)

	tlcrs := make([]tlc.TLCR, len(ports))

	for i := range ports {
		tlcrs[i] = tlc.TLCR{}
		tlcrs[i].Init(3, 1, nets[i])
	}

	var wg sync.WaitGroup

	for i := range ports {
		wg.Add(1)
		go func(id int) {
			worker(tlcrs[id], id)
			wg.Done()
		}(i)
	}

	wg.Wait()

	time.Sleep(200 * time.Microsecond)
	for i := range tlcrs {
		tlcrs[i].End()
	}

	for i := range ports {
		nets[i].StopServer()
	}

}
