all: bin/qsc

bin/qsc: cmd/main.go pkg
	mkdir -p bin
	go build -o bin/qsc cmd/main.go

.PHONY: clean
clean:
	rm -rf bin

.PHONY: protocompile
protocompile:
	cd pkg/rpc && protoc --gofast_out=. network.proto && cd ../../
	cd pkg/rpc && protoc --gofast_out=. tlc.proto && cd ../../ 