module qsc

go 1.17

require (
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/protobuf v3.19.2+incompatible // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
